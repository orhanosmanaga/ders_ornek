<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class KayitRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */

     public function attributes()
     {
         return[
             'kad'=>'kull'



         ];


     }

     public function messages()
     {
         return[
             'ad.required'=>'ad boş kalamaz'



         ];


     }


    public function rules()
    {
        return [
            'ad'=>'required',
            'kad'=>'required|min:3',
            'mail'=>'required|email',
            'sifre'=>'required|min:6|confirmed'
        ];
    }
}
