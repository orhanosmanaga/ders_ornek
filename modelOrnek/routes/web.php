<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('uye','App\Http\Controllers\Controller@index');
Route::get('uye/{id}','App\Http\Controllers\Controller@uye');
Route::get('uyeekle','App\Http\Controllers\Controller@uyeEkle');
Route::get('uyeguncelle','App\Http\Controllers\Controller@uyeGuncelle');
Route::get('uyesil','App\Http\Controllers\Controller@uyeSil');
Route::get('modelornek','App\Http\Controllers\Controller@modelOrnek');