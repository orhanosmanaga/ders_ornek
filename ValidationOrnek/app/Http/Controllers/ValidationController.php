<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Validator;

class ValidationController extends Controller
{
    public function index()
    {

        return view('kayit');
    }

    public function kayitpost(Request $gelen)
    {

        /*

        //birinci yöntem

        $gelen->validate([
            'ad'=>'required',
            'kad'=>'required|min:3',
            'mail'=>'required|email',
            'sifre'=>'required|min:6|confirmed'

        
        ]);

        */


        //ikinci yöntem
        $kural=Validator::make($gelen->all(),
        [
            'ad'=>'required',
            'kad'=>'required|min:3',
            'mail'=>'required|email',
            'sifre'=>'required|min:6|confirmed'

        ],
        [
            'min'=>':attribute en az :min karakter olmalı'



        ]
    
    
        );
        if($kural->fails())
        {return redirect()->back()->withErrors($kural)->withInput();}
        else
        {return "tamam";}
        
    }
}
